# escape=`
# FROM mcr.microsoft.com/windows/servercore/iis:windowsservercore-1709
FROM mcr.microsoft.com/windows/servercore:1709
# Install Chocolatey
RUN @powershell -NoProfile -ExecutionPolicy Bypass -Command "$env:ChocolateyUseWindowsCompression='false'; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

# Install build tools
RUN powershell add-windowsfeature web-asp-net45 ; `
    choco install microsoft-build-tools -y --allow-empty-checksums -version 14.0.23107.10 ; `
    choco install dotnet4.6-targetpack --allow-empty-checksums -y  ; `
    choco install nuget.commandline --allow-empty-checksums -y ; `
    choco install netfx-4.5.1-devpack --allow-empty-checksums -y ; `
    choco install git -params '"/GitAndUnixToolsOnPath"' --allow-empty-checksums -y; `
    nuget install MSBuild.Microsoft.VisualStudio.Web.targets -Version 14.0.0.3 ; `
    nuget install WebConfigTransformRunner -Version 1.0.0.1


RUN powershell nuget install NUnit -Version 3.8.1 ; `
               nuget install NUnit3TestAdapter -Version 3.8.0 ; `
               nuget install NUnit.ConsoleRunner -Version 3.9.0 ; `
               nuget install NUnit.Extension.NUnitProjectLoader -Version 3.6.0 ; `
               nuget install NUnit.Extension.NUnitV2Driver -Version 3.7.0 ; `
               nuget install NUnit.Extension.NUnitV2ResultWriter -Version 3.6.0 ; `
               nuget install NUnit.Extension.TeamCityEventListener -Version 1.0.4 ; `
               nuget install NUnit.Extension.VSProjectLoader --version 3.8.0 ; `
               nuget install NUnit.Console -Version 3.9.0


RUN powershell remove-item C:\inetpub\wwwroot\iisstart.*

# Get and Install Java
RUN powershell -Command `
    wget 'http://javadl.oracle.com/webapps/download/AutoDL?BundleId=210185' -Outfile 'C:\jreinstaller.exe' ; `
    Start-Process -filepath C:\jreinstaller.exe -passthru -wait -argumentlist "/s" ; `
    del C:\jreinstaller.exe

ENV JAVA_HOME c:\\Program Files\\Java\\jre1.8.0_91
RUN setx PATH "%path%;C:\\Program Files\\Java\\jre1.8.0_91\\bin"
RUN setx PATH "%path%;C:\\Program Files (x86)\\MSBuild\\14.0\\Bin"
RUN setx PATH "%path%;C:\\NUnit.ConsoleRunner.3.9.0\\tools"
#RUN setx PATH "C:\\NUnit.ConsoleRunner.3.9.0\\tools"

 CMD [ "java.exe" ]

 SHELL ["powershell"]
 ARG BASE_URL
 ARG SECRET

 RUN mkdir C:\Jenkins

 ENTRYPOINT powershell
#  RUN wget 'http://35.184.2.108/jenkins/jnlpJars/agent.jar' -Outfile 'C:\\Jenkins\\slave.jar'

#  ENTRYPOINT ["C:\\Program Files\\Java\\jre1.8.0_91\\bin\\java.exe", "-jar", "C:\\Jenkins\\slave.jar"]
